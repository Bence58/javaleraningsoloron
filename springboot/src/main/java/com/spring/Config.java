package com.spring;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Component;

@Configuration
@EntityScan(basePackages = "com.spring.entities")
@EnableJpaRepositories(basePackages = "com.spring.repository")
public class Config {

}
