package com.spring.repository;

import com.spring.entities.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class AuthorRepository {

    @Autowired
    private EntityManager em;
    @Transactional
    public void saveAuthor(Author author) {
        em.persist(author);
    }
@Transactional
    public List<Author> getAllAuthor() {
        Query q = em.createQuery("from Author");
        return q.getResultList();
    }


}
