package com.spring.repository;

import com.spring.entities.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
public class BookRepository {

    @Autowired
    private EntityManager em;
    @Transactional
    public void saveBook(Book book){
        em.persist(book);
    }


}
