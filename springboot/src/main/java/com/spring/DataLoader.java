package com.spring;

import com.spring.entities.Author;
import com.spring.entities.Book;
import com.spring.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class DataLoader implements ApplicationRunner {
    @Autowired
    private AuthorRepository ar;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        List booklist = new ArrayList();
        booklist.add(new Book(null,"VALAMI konyv",200, Date.valueOf(LocalDate.now())));
        booklist.add(new Book(null,"VALAMI konyv2",201, Date.valueOf(LocalDate.now())));
        booklist.add(new Book(null,"VALAMI konyv3",202, Date.valueOf(LocalDate.now())));

        List booklist2 = new ArrayList();
        booklist2.add(new Book(null,"VALAMI konyv",500, Date.valueOf(LocalDate.now())));
        booklist2.add(new Book(null,"VALAMI konyv2",2432, Date.valueOf(LocalDate.now())));
        booklist2.add(new Book(null,"VALAMI konyv3",2042, Date.valueOf(LocalDate.now())));

        ar.saveAuthor(new Author(null,"Janos",booklist));
        ar.saveAuthor(new Author(null,"FHGFffdsf",booklist2));
    }
}
