package com.spring.controller;

import com.spring.entities.Author;
import com.spring.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/")
public class AuthorRestController {

    @Autowired
    private AuthorService as;

    @RequestMapping(path = "authorlist", method = RequestMethod.GET)
    public ResponseEntity<List> getAuthors() {
        return ResponseEntity.ok(as.getAllAuthor());
    }

}
