package com.spring.service;

import com.spring.entities.Author;
import com.spring.entities.Book;
import com.spring.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorService {

    @Autowired
    private AuthorRepository ar;

    public void saveAuthorService(Author author) {
        ar.saveAuthor(author);
    }

    public List<Author> getAllAuthor() {
        return ar.getAllAuthor();
    }


}
