package com.spring.service;

import com.spring.entities.Book;
import com.spring.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookService {

    @Autowired
    private BookRepository br;

    public void saveBookService(Book book){
        br.saveBook(book);
    }

}
